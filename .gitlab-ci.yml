# This file is part of sv-witnesses repository: https://github.com/sosy-lab/sv-witnesses
#
# SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

default:
  image: python:3.11

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

.pip-cache: &pip-cache
  cache:
    paths:
      - .cache/pip

# Linter checks
black:
  before_script:
    - pip install black
  script:
    - black lint --check --diff
  <<: *pip-cache

flake8:
  before_script:
    - "pip install 'flake8<6,>=3.7' flake8-awesome"
  script:
    - flake8
  <<: *pip-cache

linter-tests:
  before_script:
    - pip install pytest coverage
    - pip install -r requirements.txt
  script:
    - coverage run --source=lint -m pytest lint
    - coverage report
    - coverage html
  artifacts:
    paths:
      - htmlcov/
    when: always
  <<: *pip-cache

# Check license headers
reuse:
  image:
    name: fsfe/reuse:latest
    entrypoint: [""]
  script:
    - reuse lint

# Check whether example files pass linter
lint-examples:
  before_script:
    - pip install -r requirements.txt
  script:
    - ./lint-examples.sh

pages:
  <<: *pip-cache
  before_script:
    - pip install json-schema-for-humans==0.41.8
  script:
    - mkdir -p public/yaml/
    - generate-schema-doc --config-file jsfh.yml correctness-witness-schema.yml public/yaml/correctness-witnesses.html
    - generate-schema-doc --config-file jsfh.yml violation-witness-schema.yml public/yaml/violation-witnesses.html
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
