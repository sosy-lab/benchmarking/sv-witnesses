<!--
This file is part of sv-witnesses repository: https://github.com/sosy-lab/sv-witnesses

SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

Changes from version 2.0.1 to version 2.0.2
------------------------------------------

* `C` is no longer supported as format for constraints in violation witnesses version 2.0.
* Testing the BenchExec integration is now supported

Changes from version 2.0 to version 2.0.1
------------------------------------------
* Added option to exit successfully even if the witness syntax contains faults