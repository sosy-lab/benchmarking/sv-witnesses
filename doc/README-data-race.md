<!--
This file is part of sv-witnesses repository: https://github.com/sosy-lab/sv-witnesses

SPDX-FileCopyrightText: 2024 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# Violation Witnesses for Data Races

In the best case, a violation witness for a data race is a minimal interleaving ending with the two actions (of the two different threads) that constitute a race (in whichever order since neither happens-before the other).
That is, a well-defined concurrent execution up to the first undefined behavior in the form of a data race (similar to no-overflow or valid-memsafety violations).

However, the automaton-based violation witness is not required to be as precise: it could also describe some prefix of an execution from which a data race could be reached.
More generally, the automaton that restricts the state space for the validator, just like automaton-based violation witnesses for single-threaded programs or reachability in multi-threaded programs.

## Exchange Format

The witness exchange format is the [GraphML-Based Exchange Format for Violation Witnesses, version 1.0](./README-GraphML.md).
The additional edge data keys `threadId` and `createThread` (described in the linked document) are used for concurrency information.

## Validators

The following provides additional details on techniques used by _some_ validators for data race violations.
This can be useful for generating witnesses which a validator benefits from, but should not be considered a complete and up-to-date reference.

### Dartagnan

Hernan Ponce de Leon ([originally](https://gitlab.com/sosy-lab/sv-comp/bench-defs/-/merge_requests/452#note_2169739276), [improved](https://gitlab.com/sosy-lab/benchmarking/sv-witnesses/-/merge_requests/85)):

> Dartagnan interprets the violation witness as a linearization of a happens before (HB) relation and tries to find two conflicting instructions in different threads (same memory location, at least one a write, at least one non-atomic) that are next to the other.
> The traditional definition of races requires events (i.e., instances of instructions) not to be HB-ordered.
> This is guaranteed in Dartagnan because any proper synchronization (mutex, atomic block) would put an event in between them.
>
> The conflicting pair could be anywhere in the HB order, i.e. Dartagnan does not assume the witness ends as soon as the race happens.
