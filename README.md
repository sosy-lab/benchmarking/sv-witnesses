<!--
This file is part of sv-witnesses repository: https://github.com/sosy-lab/sv-witnesses

SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# Exchange Format for Violation Witnesses and Correctness Witnesses

## Contents:
* [GraphML-Based Exchange Format for Violation Witnesses and Correctness Witnesses](doc/README-GraphML.md), version 1.0
    * [Violation Witnesses for Termination](./termination/README.md)
    * [Violation Witnesses for Data Races](./doc/README-data-race.md)
* [YAML-Based Exchange Format for Correctness Witnesses](doc/README-YAML.md), Draft
* [YAML-Based Exchange Format for Correctness Witnesses](https://sosy-lab.gitlab.io/benchmarking/sv-witnesses/yaml/correctness-witnesses.html), Version 2.0
* [YAML-Based Exchange Format for Violation Witnesses](https://sosy-lab.gitlab.io/benchmarking/sv-witnesses/yaml/violation-witnesses.html), Version 2.0
* [Literature](doc/README-Literature.md)
* [WitnessLint, a linter for verification witnesses](lint/README.md)

