#!/bin/bash

# This file is part of sv-witnesses repository: https://github.com/sosy-lab/sv-witnesses
#
# SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

error=0
echo "Linting GraphML examples"
found_at_least_one=0
for file in $(find examples -iwholename "*.[ci]"); do
  for witness in $(find examples -iwholename "${file%%.*}*.graphml"); do
    found_at_least_one=1
    ./lint/witnesslinter.py $file --witness $witness || error=1
  done
done

if [ $found_at_least_one -eq 0 ]; then
  echo "No GraphML files found"
  exit 1
fi

echo "Linting YAML examples"
found_at_least_one=0
for file in $(find examples -iwholename "*.[ci]"); do
  for witness in $(find examples -iwholename "${file%%.*}*witness.yml"); do
    found_at_least_one=1
    ./lint/witnesslinter.py $file --witness $witness || error=1
  done
done

if [ $found_at_least_one -eq 0 ]; then
  echo "No YAML files found"
  exit 1
fi

exit $error
