# This file is part of sv-witnesses repository: https://github.com/sosy-lab/sv-witnesses
#
# SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import os.path
from pathlib import Path

CORRECTNESS_WITNESS_SCHEMA_PATH = os.path.join(
    Path(__file__).parent, "../../../correctness-witness-schema.yml"
)
VIOLATION_WITNESS_SCHEMA_PATH = os.path.join(
    Path(__file__).parent, "../../../violation-witness-schema.yml"
)
