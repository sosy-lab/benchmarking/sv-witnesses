# This file is part of sv-witnesses repository: https://github.com/sosy-lab/sv-witnesses
#
# SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import yaml
from jsonschema import validate
from jsonschema.exceptions import ValidationError, SchemaError

from lint.witnesslint import logger as logging
from lint.witnesslint.constants import (
    WitnessType,
    WITNESS_FAULTY,
    INTERNAL_ERROR,
)
from lint.witnesslint.linter import Linter
from lint.witnesslint.utils import _exit
from lint.witnesslint.yaml_linter.constants import (
    CORRECTNESS_WITNESS_SCHEMA_PATH,
    VIOLATION_WITNESS_SCHEMA_PATH,
)


class YAMLWitnessLinter(Linter):
    def __init__(self, witness, program, options):
        self.witness = witness
        if program is not None:
            self.program_contents = open(program, "r").read()
        else:
            self.program_contents = None
        self.options = options

    def matches_schema(self):
        if self.witness.witness_type == WitnessType.CORRECTNESS:
            try:
                validate(
                    self.witness.witness_entries,
                    yaml.safe_load(open(CORRECTNESS_WITNESS_SCHEMA_PATH, mode="r")),
                )
            except ValidationError as e:
                logging.critical(
                    "Correctness Witness does not match the schema.\n{}".format(e)
                )
                _exit(WITNESS_FAULTY)
            except SchemaError as e:
                logging.critical(
                    "Error in the schema for correctness witnesses.\n{}".format(e)
                )
                _exit(INTERNAL_ERROR)
        elif self.witness.witness_type == WitnessType.VIOLATION:
            try:
                validate(
                    self.witness.witness_entries,
                    yaml.safe_load(open(VIOLATION_WITNESS_SCHEMA_PATH, mode="r")),
                )
            except ValidationError as e:
                logging.critical(
                    "Correctness Witness does not match the schema.\n{}".format(e)
                )
                _exit(WITNESS_FAULTY)
            except SchemaError as e:
                logging.critical(
                    "Error in the schema for correctness witnesses.\n{}".format(e)
                )
                _exit(INTERNAL_ERROR)
        else:
            logging.critical("Witness type could not be determined.")
            _exit(INTERNAL_ERROR)
        return

    def invariants_at_feasible_locations(self):
        if self.witness.witness_type != WitnessType.CORRECTNESS:
            return

        if self.program_contents is None:
            return

        program_lines = self.program_contents.split("\n")
        witness_faulty = False

        for e in self.witness.witness_entries:
            for c in e["content"]:
                entry = c["invariant"]
                line_number = entry["location"]["line"]
                if "column" in entry["location"]:
                    column = entry["location"]["column"]
                else:
                    column = None

                # Invariants and columns start being indexed at 1
                if 0 <= line_number <= len(program_lines):
                    line = program_lines[line_number - 1]
                    line = line.replace("\t", " " * 8)
                    if column is not None and not (0 <= column <= len(line)):
                        witness_faulty = True
                else:
                    witness_faulty = True

                if witness_faulty:
                    logging.critical(
                        # We make use of implicit concatenation
                        # in order to not exceed the 82 character limit
                        (
                            "Invariant at line {} column {} "
                            "is not at a feasible location."
                        ).format(line_number, column)
                    )
                    _exit(WITNESS_FAULTY)

        return

    def waypoints_at_feasible_locations(self):
        if self.witness.witness_type != WitnessType.VIOLATION:
            return

        program_lines = self.program_contents.split("\n")
        witness_faulty = False

        for entry in self.witness.witness_entries:
            for segment in entry["content"]:
                for waypoint in segment["segment"]:
                    waypoint = waypoint["waypoint"]
                    line_number = waypoint["location"]["line"]

                    if "column" in waypoint["location"]:
                        column = waypoint["location"]["column"]
                    else:
                        column = None

                    # Waypoints and columns start being indexed at 1
                    if 0 <= line_number <= len(program_lines):
                        line = program_lines[line_number - 1]
                        line = line.replace("\t", " " * 8)
                        if column is not None and not (0 <= column <= len(line)):
                            witness_faulty = True
                    else:
                        witness_faulty = True

                    if witness_faulty:
                        logging.critical(
                            # We make use of implicit concatenation
                            # in order to not exceed the 82 character limit
                            (
                                "Waypoint at line {} column {} "
                                "is not at a feasible location."
                            ).format(line_number, column)
                        )
                        _exit(WITNESS_FAULTY)

        return

    def matches_file_name_schema(self):
        if not self.witness.witness_file.endswith(
            ".yml"
        ) and not self.witness.witness_file.endswith(".yml.gz"):
            logging.critical("Witness file name does not match the schema.")
            _exit(WITNESS_FAULTY)
        return

    def lint(self):
        self.matches_file_name_schema()
        self.matches_schema()
        if self.program_contents is not None:
            self.invariants_at_feasible_locations()
            self.waypoints_at_feasible_locations()
        self.witness.print_witness_statistics()
