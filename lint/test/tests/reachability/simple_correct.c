// This file is part of sv-witnesses repository: https://github.com/sosy-lab/sv-witnesses
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

void reach_error(){}
int main() {
  int i = 0;
  while (i<10) {
    i++;
  }
  if (i<10) {
    reach_error();
  }
  return 0;
}
