// This file is part of sv-witnesses repository: https://github.com/sosy-lab/sv-witnesses
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

void reach_error(){}

void main() {
  int x = 0;
  while (x >= 0) {
    x--;
  }
  reach_error();
}
