# This file is part of sv-witnesses repository: https://github.com/sosy-lab/sv-witnesses
#
# SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import hashlib
import itertools
import logging
from pathlib import Path
from typing import List
import sys

from lint.witnesslint.witnesslint import create_arg_parser

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

import lxml.etree
import pytest

from lint.witnesslint import witnesslint
from lint.witnesslint.graphml_linter.graphml_witness import (
    WITNESS_TYPE,
    SOURCECODELANG,
    PRODUCER,
    SPECIFICATION,
    PROGRAMFILE,
    PROGRAMHASH,
    ARCHITECTURE,
    CREATIONTIME,
    ENTRY,
    SINK,
    ASSUMPTION,
    CYCLEHEAD,
)

TEST_DIRECTORIES = [
    Path(__file__).parent.joinpath("tests"),
]
EXAMPLE_DIRECTORIES = [Path(__file__).parent.parent.parent.joinpath("examples")]

PROGRAMS_WITHOUT_YAML_WITNESSES = [
    Path(__file__).parent.joinpath("tests").joinpath("termination")
]
# A witness is required to create a WitnessLinter instance.
# Use WITNESS for tests that do not need access to a witness.
WITNESS = (
    TEST_DIRECTORIES[0]
    .joinpath("reachability")
    .joinpath("simple_correct.yml.valid.graphml")
)
DUMMY_POSITION = 1


def create_config(
    witness=WITNESS, program=None, strict_checking=False, ignore_self_loops=False
):
    argv = ["--witness", str(witness), "--loglevel", "warning"]
    if strict_checking:
        argv.append("--strictChecking")
    if ignore_self_loops:
        argv.append("--ignoreSelfLoops")
    if program is not None:
        argv.append(str(program))
    return argv


def create_linter(
    witness=WITNESS, program=None, strict_checking=False, ignore_self_loops=False
):
    argv = create_config(witness, program, strict_checking, ignore_self_loops)
    arg_parser = create_arg_parser()
    parsed_args = arg_parser.parse_args(argv)
    return witnesslint.create_linter(parsed_args)


def check_log(caplog, level, should_log):
    records = [
        record for record in caplog.get_records("call") if record.levelno >= level
    ]
    if should_log:
        assert records
    else:
        assert not records


def _get_programs(directories: List[Path], sub_directories=True) -> List[Path]:
    """
    Return a list of all programs in the given directory.
    """
    if sub_directories:
        glob_str = "*/*.[ci]"
    else:
        glob_str = "*.[ci]"

    return list(
        itertools.chain(*[program_dir.glob(glob_str) for program_dir in directories])
    )


def get_test_programs() -> List[Path]:
    return _get_programs(TEST_DIRECTORIES)


def get_example_programs() -> List[Path]:
    return _get_programs(EXAMPLE_DIRECTORIES, sub_directories=False)


def add_required_data(witness_linter, correctness_witness=True, exclude=None):
    if exclude is None:
        exclude = []

    if WITNESS_TYPE not in exclude:
        if correctness_witness:
            witness_type = lxml.etree.fromstring(
                '<data key="witness_type">correctness_witness</data>'
            )
        else:
            witness_type = lxml.etree.fromstring(
                '<data key="witness_type">violation_witness</data>'
            )
        witness_linter.handle_graph_data(witness_type, WITNESS_TYPE)

    if SOURCECODELANG not in exclude:
        sourcecodelang = lxml.etree.fromstring('<data key="sourcecodelang">C</data>')
        witness_linter.handle_graph_data(sourcecodelang, SOURCECODELANG)

    if PRODUCER not in exclude:
        producer = lxml.etree.fromstring('<data key="producer">ACSL2Witness</data>')
        witness_linter.handle_graph_data(producer, PRODUCER)

    if SPECIFICATION not in exclude:
        spec = "CHECK( init(main()), LTL(G ! call(reach_error())) )"
        specification = lxml.etree.fromstring(
            '<data key="specification">' + spec + "</data>"
        )
        witness_linter.handle_graph_data(specification, SPECIFICATION)

    if PROGRAMFILE not in exclude:
        programfile = lxml.etree.fromstring(
            '<data key="programfile">program/simple_correct.c</data>'
        )
        witness_linter.handle_graph_data(programfile, PROGRAMFILE)

    if PROGRAMHASH not in exclude:
        hashcode = "6cdd5de91d235e299713c37e9923d9f915462f180efbd0de6463b3ca98dbee73"
        programhash = lxml.etree.fromstring(
            '<data key="programhash">' + hashcode + "</data>"
        )
        witness_linter.handle_graph_data(programhash, PROGRAMHASH)

    if ARCHITECTURE not in exclude:
        architecture = lxml.etree.fromstring('<data key="architecture">32bit</data>')
        witness_linter.handle_graph_data(architecture, ARCHITECTURE)

    if CREATIONTIME not in exclude:
        creationtime = lxml.etree.fromstring(
            '<data key="creationtime">2020-11-23T15:03:32+01:00</data>'
        )
        witness_linter.handle_graph_data(creationtime, CREATIONTIME)

    if ENTRY not in exclude:
        key = '<key for="node" id="entry"/>'
        key_declaration = lxml.etree.fromstring(key)
        witness_linter.handle_key(key_declaration)
        entrynode = lxml.etree.fromstring(
            '<node id="ENTRY"><data key="entry">true</data></node>'
        )
        witness_linter.handle_node(entrynode)


def test_main_returns_expected_result_for_graphml():
    for program in get_test_programs():
        witness = program.with_suffix(".yml.valid.graphml")
        if witness.exists():
            argv = create_config(witness, program)
            with pytest.raises(SystemExit) as exc_info:
                witnesslint.main(argv)
            assert exc_info.value.code == 0

        witness = program.with_suffix(".yml.faulty.graphml")
        if witness.exists():
            argv = create_config(witness, program)
            with pytest.raises(SystemExit) as exc_info:
                witnesslint.main(argv)
            assert exc_info.value.code == 1


def test_main_returns_expected_result_for_examples_graphml():
    for program in get_example_programs():
        witness = program.with_suffix(".graphml")
        if witness.exists():
            argv = create_config(witness, program)
            with pytest.raises(SystemExit) as exc_info:
                witnesslint.main(argv)
            assert exc_info.value.code == 0
        else:
            pytest.fail("No GraphML witness found for {}".format(program))


def test_main_returns_expected_result_for_examples_yaml():
    for program in get_example_programs():
        witness = program.with_suffix(".witness.yml")
        if witness.exists():
            argv = create_config(witness, program)
            with pytest.raises(SystemExit) as exc_info:
                witnesslint.main(argv)
            assert exc_info.value.code == 0


def test_main_returns_expected_result_for_yaml():
    for program in get_test_programs():
        if any(
            str(program).startswith(str(no_yaml_witness_directory))
            for no_yaml_witness_directory in PROGRAMS_WITHOUT_YAML_WITNESSES
        ):
            continue
        witness_valid = program.with_suffix(".yml.valid.yml")
        witness_faulty = program.with_suffix(".yml.faulty.yml")
        if not witness_valid.exists() and not witness_faulty.exists():
            pytest.fail(
                "Neither a valid nor a faulty YAML witness was found for {}".format(
                    program
                )
            )

        if witness_valid.exists():
            argv = create_config(witness_valid, program)
            with pytest.raises(SystemExit) as exc_info:
                witnesslint.main(argv)
            assert exc_info.value.code == 0

        if witness_faulty.exists():
            argv = create_config(witness_faulty, program)
            with pytest.raises(SystemExit) as exc_info:
                witnesslint.main(argv)
            assert exc_info.value.code == 1


def test_basic_program_info_available():
    for program in get_test_programs():
        for witness in [
            program.with_suffix(".yml.valid.graphml"),
        ]:
            if not witness.exists():
                continue
            witness_linter = create_linter(witness)
            witness_linter.collect_program_info(program)
            assert "name" in witness_linter.program_info
            assert "num_chars" in witness_linter.program_info
            assert "num_lines" in witness_linter.program_info
            assert "sha256_hash" in witness_linter.program_info
            assert "function_names" in witness_linter.program_info
            assert "sucessfully_parsed" in witness_linter.program_info


def test_sha256_hash_matches():
    for program in get_test_programs():
        for witness in [
            program.with_suffix(".yml.valid.graphml"),
        ]:
            if not witness.exists():
                continue
            witness_linter = create_linter(witness)
            witness_linter.collect_program_info(program)
            with open(program, "rb") as source:
                content = source.read()
                program_hash = hashlib.sha256(content).hexdigest()
            linter_computed_hash = witness_linter.program_info.get("sha256_hash")
            assert linter_computed_hash == program_hash


def test_check_linenumber_logs_warning_if_linenumber_invalid(caplog):
    for program in get_test_programs():
        for witness in [program.with_suffix(".yml.valid.graphml")]:
            if not witness.exists():
                continue
            witness_linter = create_linter(witness, strict_checking=True)
            witness_linter.collect_program_info(program)
            for invalid_linenumber in [
                -5,
                0,
                witness_linter.program_info["num_lines"] + 1,
            ]:
                witness_linter.check_linenumber(invalid_linenumber, DUMMY_POSITION)
                check_log(caplog, logging.WARNING, should_log=True)
                caplog.clear()


def test_check_linenumber_logs_no_warning_if_linenumber_valid(caplog):
    for program in get_test_programs():
        for witness in [
            program.with_suffix(".yml.valid.graphml"),
        ]:
            if not witness.exists():
                continue
            witness_linter = create_linter(witness, strict_checking=True)
            witness_linter.collect_program_info(program)
            for valid_linenumber in [1, witness_linter.program_info["num_lines"]]:
                witness_linter.check_linenumber(valid_linenumber, DUMMY_POSITION)
                check_log(caplog, logging.WARNING, should_log=False)
                caplog.clear()


def test_check_character_offset_logs_warning_if_offset_invalid(caplog):
    for program in get_test_programs():
        for witness in [
            program.with_suffix(".yml.valid.graphml"),
        ]:
            if not witness.exists():
                continue
            witness_linter = create_linter(witness, strict_checking=True)
            witness_linter.collect_program_info(program)
            for invalid_offset in [-10, -1, witness_linter.program_info["num_chars"]]:
                witness_linter.check_character_offset(invalid_offset, DUMMY_POSITION)
                check_log(caplog, logging.WARNING, should_log=True)
                caplog.clear()


def test_check_character_offset_logs_no_warning_if_offset_valid(caplog):
    for program in get_test_programs():
        for witness in [
            program.with_suffix(".yml.valid.graphml"),
        ]:
            if not witness.exists():
                continue
            witness_linter = create_linter(witness, strict_checking=True)
            witness_linter.collect_program_info(program)
            for valid_offset in [0, witness_linter.program_info["num_chars"] - 1]:
                witness_linter.check_character_offset(valid_offset, DUMMY_POSITION)
                check_log(caplog, logging.WARNING, should_log=False)
                caplog.clear()


def test_handle_graphml_logs_no_warning_if_correct_namespaces_used(caplog):
    witness_linter = create_linter()
    default_namespace = 'xmlns="http://graphml.graphdrawing.org/xmlns"'
    schema_namespace = 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'
    graphml = lxml.etree.fromstring(
        "<graphml {0} {1}/>".format(default_namespace, schema_namespace)
    )
    witness_linter.handle_graphml_elem(graphml)
    check_log(caplog, logging.WARNING, should_log=False)


def test_handle_graphml_logs_warning_if_default_namespace_missing(caplog):
    witness_linter = create_linter()
    schema_namespace = 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'
    graphml = lxml.etree.fromstring("<graphml {0}/>".format(schema_namespace))
    witness_linter.handle_graphml_elem(graphml)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_graphml_logs_warning_if_wrong_default_namespace_used(caplog):
    witness_linter = create_linter()
    default_namespace = 'xmlns="http://better.namespace.org/xmlns"'
    schema_namespace = 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'
    graphml = lxml.etree.fromstring(
        "<graphml {0} {1}/>".format(default_namespace, schema_namespace)
    )
    witness_linter.handle_graphml_elem(graphml)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_graphml_logs_warning_if_xml_schema_namespace_missing(caplog):
    witness_linter = create_linter()
    default_namespace = 'xmlns="http://graphml.graphdrawing.org/xmlns"'
    graphml = lxml.etree.fromstring("<graphml {0}/>".format(default_namespace))
    witness_linter.handle_graphml_elem(graphml)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_graphml_logs_warning_if_wrong_xml_schema_namespace_used(caplog):
    witness_linter = create_linter()
    default_namespace = 'xmlns="http://graphml.graphdrawing.org/xmlns"'
    schema_namespace = 'xmlns:xsi="http://xml.org/latest/XMLSchema"'
    graphml = lxml.etree.fromstring(
        "<graphml {0} {1}/>".format(default_namespace, schema_namespace)
    )
    witness_linter.handle_graphml_elem(graphml)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_graph_logs_warning_if_edgedefault_missing(caplog):
    witness_linter = create_linter()
    graph = lxml.etree.fromstring("<graph/>")
    witness_linter.handle_graph(graph)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_graph_logs_warning_if_wrong_edgedefault_is_used(caplog):
    witness_linter = create_linter()
    graph = lxml.etree.fromstring('<graph edgedefault="directed"/>')
    witness_linter.handle_graph(graph)
    check_log(caplog, logging.WARNING, should_log=False)
    caplog.clear()

    witness_linter = create_linter()
    graph = lxml.etree.fromstring('<graph edgedefault="undirected"/>')
    witness_linter.handle_graph(graph)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_node_logs_warning_if_node_has_multiple_attributes(caplog):
    witness_linter = create_linter()
    node = lxml.etree.fromstring('<node id="N0" entry="true"/>')
    witness_linter.handle_node(node)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_node_logs_warning_if_id_is_missing(caplog):
    witness_linter = create_linter()
    node = lxml.etree.fromstring("<node/>")
    witness_linter.handle_node(node)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_node_logs_warning_if_node_id_used_multiple_times(caplog):
    witness_linter = create_linter()

    node = lxml.etree.fromstring('<node id="N0"/>')
    witness_linter.handle_node(node)
    check_log(caplog, logging.WARNING, should_log=False)

    witness_linter.handle_node(node)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_edge_logs_warning_if_source_or_target_is_missing(caplog):
    no_source_edge = lxml.etree.fromstring('<edge target="N1"/>')
    no_target_edge = lxml.etree.fromstring('<edge source="N0"/>')
    empty_edge = lxml.etree.fromstring("<edge/>")

    for edge in [no_source_edge, no_target_edge, empty_edge]:
        witness_linter = create_linter()
        witness_linter.handle_edge(edge)
        check_log(caplog, logging.WARNING, should_log=True)
        caplog.clear()


def test_handle_edge_logs_warning_if_source_equals_target_unless_self_loops_allowed(
    caplog,
):
    self_loop = lxml.etree.fromstring('<edge source="N0" target="N0"/>')

    witness_linter = create_linter()
    witness_linter.handle_edge(self_loop)
    check_log(caplog, logging.WARNING, should_log=True)
    caplog.clear()

    witness_linter = create_linter(ignore_self_loops=True)
    witness_linter.handle_edge(self_loop)
    check_log(caplog, logging.WARNING, should_log=False)


def test_handle_graph_data_logs_warning_if_witness_type_declared_multiple_times(caplog):
    witness_linter = create_linter()
    graph_data = lxml.etree.fromstring(
        '<data key="witness-type">correctness_witness</data>'
    )

    witness_linter.handle_graph_data(graph_data, WITNESS_TYPE)
    check_log(caplog, logging.WARNING, should_log=False)
    caplog.clear()

    witness_linter.handle_graph_data(graph_data, WITNESS_TYPE)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_graph_data_logs_warning_if_sourcecode_lang_declared_multiple_times(
    caplog,
):
    witness_linter = create_linter()
    graph_data = lxml.etree.fromstring('<data key="sourcecodelang">C</data>')

    witness_linter.handle_graph_data(graph_data, SOURCECODELANG)
    check_log(caplog, logging.WARNING, should_log=False)
    caplog.clear()

    witness_linter.handle_graph_data(graph_data, SOURCECODELANG)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_graph_data_logs_warning_if_producer_declared_multiple_times(caplog):
    witness_linter = create_linter()
    graph_data = lxml.etree.fromstring('<data key="producer">H.U.M.A.N.</data>')

    witness_linter.handle_graph_data(graph_data, PRODUCER)
    check_log(caplog, logging.WARNING, should_log=False)
    caplog.clear()

    witness_linter.handle_graph_data(graph_data, PRODUCER)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_graph_data_logs_warning_if_program_file_declared_multiple_times(caplog):
    witness_linter = create_linter()
    graph_data = lxml.etree.fromstring(
        '<data key="programfile">program/simple/simple_correct.c</data>'
    )

    witness_linter.handle_graph_data(graph_data, PROGRAMFILE)
    check_log(caplog, logging.WARNING, should_log=False)
    caplog.clear()

    witness_linter.handle_graph_data(graph_data, PROGRAMFILE)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_graph_data_logs_warning_if_program_hash_declared_multiple_times(caplog):
    witness_linter = create_linter()
    hashcode = "cce9dad5189ba10b168f15bbbb9be82d3acfd6a0e92294166e1b9f88994172b6"
    graph_data = lxml.etree.fromstring(
        '<data key="programhash">' + hashcode + "</data>"
    )

    witness_linter.handle_graph_data(graph_data, PROGRAMHASH)
    check_log(caplog, logging.WARNING, should_log=False)
    caplog.clear()

    witness_linter.handle_graph_data(graph_data, PROGRAMHASH)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_graph_data_logs_warning_if_architecture_declared_multiple_times(caplog):
    witness_linter = create_linter()
    graph_data = lxml.etree.fromstring('<data key="architecture">32bit</data>')

    witness_linter.handle_graph_data(graph_data, ARCHITECTURE)
    check_log(caplog, logging.WARNING, should_log=False)
    caplog.clear()

    witness_linter.handle_graph_data(graph_data, ARCHITECTURE)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_graph_data_logs_warning_if_creation_time_declared_multiple_times(
    caplog,
):
    witness_linter = create_linter()
    graph_data = lxml.etree.fromstring(
        '<data key="creationtime">2023-03-22T15:08:32Z</data>'
    )

    witness_linter.handle_graph_data(graph_data, CREATIONTIME)
    check_log(caplog, logging.WARNING, should_log=False)
    caplog.clear()

    witness_linter.handle_graph_data(graph_data, CREATIONTIME)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_graph_data_logs_warning_if_undefined_key_is_used(caplog):
    witness_linter = create_linter()
    graph_data = lxml.etree.fromstring('<data key="mycustomkey">great_value</data>')

    witness_linter.handle_graph_data(graph_data, "mycustomkey")
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_node_data_logs_warning_if_multiple_entry_nodes_present(caplog):
    witness_linter = create_linter()

    n0 = lxml.etree.fromstring('<node id="N0"><data key="entry">true</data></node>')
    n1 = lxml.etree.fromstring('<node id="N1"><data key="entry">true</data></node>')

    for child in n0:
        witness_linter.handle_node_data(child, ENTRY, n0)
    for child in n1:
        witness_linter.handle_node_data(child, ENTRY, n1)

    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_node_data_logs_warning_on_invalid_value_for_key_entry(caplog):
    witness_linter = create_linter()

    node = lxml.etree.fromstring(
        '<node id="N"><data key="entry">sometimes</data></node>'
    )

    for child in node:
        witness_linter.handle_node_data(child, ENTRY, node)

    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_node_data_logs_warning_if_multiple_cycleheads_present(caplog):
    witness_linter = create_linter()

    n0 = lxml.etree.fromstring('<node id="N0"><data key="cyclehead">true</data></node>')
    n1 = lxml.etree.fromstring('<node id="N1"><data key="cyclehead">true</data></node>')

    for child in n0:
        witness_linter.handle_node_data(child, CYCLEHEAD, n0)
    for child in n1:
        witness_linter.handle_node_data(child, CYCLEHEAD, n1)

    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_node_data_logs_warning_if_sink_node_has_leaving_edges(caplog):
    witness_linter = create_linter()

    sink = lxml.etree.fromstring('<node id="S"><data key="sink">true</data></node>')
    successor = lxml.etree.fromstring('<node id="N"/>')
    edge = lxml.etree.fromstring('<edge id="E" source="S" target="N"/>')

    witness_linter.handle_node(successor)
    witness_linter.handle_edge(edge)
    for child in sink:
        witness_linter.handle_node_data(child, SINK, sink)

    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_node_data_logs_warning_if_undefined_key_is_used(caplog):
    witness_linter = create_linter()
    data = '<data key="mycustomkey">awesome_value</data>'
    node = lxml.etree.fromstring('<node id="N">' + data + "</node>")

    for child in node:
        witness_linter.handle_node_data(child, "mycustomkey", node)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_edge_data_logs_warning_if_result_used_without_resultfunction(caplog):
    witness_linter = create_linter()

    assumption = r'<data key="assumption">\result==0</data>'
    edge = lxml.etree.fromstring(
        '<edge id="E" source="N0" target="N1">' + assumption + "</edge>"
    )

    for child in edge:
        witness_linter.handle_edge_data(child, ASSUMPTION, edge)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_edge_data_logs_warning_if_undefined_key_is_used(caplog):
    witness_linter = create_linter()

    data = '<data key="mycustomkey">fantastic_value</data>'
    edge = lxml.etree.fromstring(
        '<edge id="E" source="N0" target="N1">' + data + "</edge>"
    )

    for child in edge:
        witness_linter.handle_edge_data(child, "mycustomkey", edge)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_key_logs_warning_if_key_id_is_missing(caplog):
    witness_linter = create_linter()
    key = '<key for="edge"/>'
    key_declaration = lxml.etree.fromstring(key)
    witness_linter.handle_key(key_declaration)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_key_logs_warning_if_key_domain_is_missing(caplog):
    witness_linter = create_linter()
    key = '<key id="threadId"/>'
    key_declaration = lxml.etree.fromstring(key)
    witness_linter.handle_key(key_declaration)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_key_logs_warning_if_key_defined_multiple_times(caplog):
    witness_linter = create_linter()

    key = '<key for="edge" id="threadId"/>'
    key_declaration = lxml.etree.fromstring(key)
    witness_linter.handle_key(key_declaration)
    check_log(caplog, logging.WARNING, should_log=False)

    witness_linter.handle_key(key_declaration)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_key_logs_warning_if_common_key_used_for_wrong_domain(caplog):
    witness_linter = create_linter()
    key = '<key for="node" id="threadId"/>'
    key_declaration = lxml.etree.fromstring(key)
    witness_linter.handle_key(key_declaration)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_key_logs_warning_if_key_has_multiple_children(caplog):
    witness_linter = create_linter()
    key = '<key for="node" id="sink"><default>false</default><const>true</const></key>'
    key_declaration = lxml.etree.fromstring(key)
    witness_linter.handle_key(key_declaration)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_key_logs_warning_if_key_default_has_attrib(caplog):
    witness_linter = create_linter()
    key = '<key for="node" id="sink"><default for="node">false</default></key>'
    key_declaration = lxml.etree.fromstring(key)
    witness_linter.handle_key(key_declaration)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_key_logs_warning_if_key_default_empty(caplog):
    witness_linter = create_linter()
    key = '<key for="node" id="sink"><default/></key>'
    key_declaration = lxml.etree.fromstring(key)
    witness_linter.handle_key(key_declaration)
    check_log(caplog, logging.WARNING, should_log=True)


def test_handle_key_logs_warning_if_wrong_default_is_used(caplog):
    default = "<default>true</default>"
    entry = '<key for="node" id="entry">' + default + "</key>"
    sink = '<key for="node" id="sink">' + default + "</key>"
    violation = '<key for="node" id="violation">' + default + "</key>"
    enter_loop_head = '<key for="edge" id="enterLoopHead">' + default + "</key>"

    for key in [entry, sink, violation, enter_loop_head]:
        witness_linter = create_linter()
        key_declaration = lxml.etree.fromstring(key)
        witness_linter.handle_key(key_declaration)
        check_log(caplog, logging.WARNING, should_log=True)
        caplog.clear()


def test_final_checks_logs_warning_if_required_data_missing(caplog):
    witness_linter = create_linter()
    add_required_data(witness_linter)
    witness_linter.final_checks()
    check_log(caplog, logging.WARNING, should_log=False)

    for required in [
        WITNESS_TYPE,
        SOURCECODELANG,
        PRODUCER,
        SPECIFICATION,
        PROGRAMFILE,
        PROGRAMHASH,
        ARCHITECTURE,
        CREATIONTIME,
    ]:
        caplog.clear()
        witness_linter = create_linter()
        add_required_data(witness_linter, exclude=[required])
        witness_linter.final_checks()
        check_log(caplog, logging.WARNING, should_log=True)

    caplog.clear()
    witness_linter = create_linter()
    add_required_data(witness_linter, exclude=[ENTRY])
    witness_linter.final_checks()
    check_log(caplog, logging.WARNING, should_log=False)

    caplog.clear()
    witness_linter = create_linter()
    add_required_data(witness_linter, exclude=[ENTRY])
    node = lxml.etree.fromstring('<node id="N0"/>')
    witness_linter.handle_node(node)
    witness_linter.final_checks()
    check_log(caplog, logging.WARNING, should_log=True)


def test_final_checks_logs_warning_if_sink_used_in_correctness_witness(caplog):
    witness_linter = create_linter()
    add_required_data(witness_linter, correctness_witness=True)

    key = '<key for="node" id="sink"/>'
    key_declaration = lxml.etree.fromstring(key)
    witness_linter.handle_key(key_declaration)

    node = lxml.etree.fromstring('<node id="N"><data key="sink">true</data></node>')
    witness_linter.handle_node(node)
    check_log(caplog, logging.WARNING, should_log=False)

    witness_linter.final_checks()
    check_log(caplog, logging.WARNING, should_log=True)


def test_final_checks_logs_warning_if_violation_used_in_correctness_witness(caplog):
    witness_linter = create_linter()
    add_required_data(witness_linter, correctness_witness=True)

    key = '<key for="node" id="violation"/>'
    key_declaration = lxml.etree.fromstring(key)
    witness_linter.handle_key(key_declaration)

    node = lxml.etree.fromstring(
        '<node id="N"><data key="violation">true</data></node>'
    )
    witness_linter.handle_node(node)
    check_log(caplog, logging.WARNING, should_log=False)

    witness_linter.final_checks()
    check_log(caplog, logging.WARNING, should_log=True)


def test_final_checks_logs_warning_if_invariant_used_in_violation_witness(caplog):
    witness_linter = create_linter()
    add_required_data(witness_linter, correctness_witness=False)

    key = '<key for="node" id="invariant"/>'
    key_declaration = lxml.etree.fromstring(key)
    witness_linter.handle_key(key_declaration)

    node = lxml.etree.fromstring(
        '<node id="N"><data key="invariant">x==0</data></node>'
    )
    witness_linter.handle_node(node)
    check_log(caplog, logging.WARNING, should_log=False)

    witness_linter.final_checks()
    check_log(caplog, logging.WARNING, should_log=True)


def test_final_checks_logs_warning_if_invariant_scope_used_in_violation_witness(caplog):
    witness_linter = create_linter()
    add_required_data(witness_linter, correctness_witness=False)

    key = '<key for="node" id="invariant.scope"/>'
    key_declaration = lxml.etree.fromstring(key)
    witness_linter.handle_key(key_declaration)

    node = lxml.etree.fromstring(
        '<node id="N"><data key="invariant.scope">main</data></node>'
    )
    witness_linter.handle_node(node)
    check_log(caplog, logging.WARNING, should_log=False)

    witness_linter.final_checks()
    check_log(caplog, logging.WARNING, should_log=True)
