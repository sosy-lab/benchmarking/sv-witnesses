#!/usr/bin/env python3

# This file is part of sv-witnesses repository: https://github.com/sosy-lab/sv-witnesses
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import sys
from pathlib import Path

# Add the directory containing the lint package to the path
sys.path.append(str(Path(__file__).absolute().parent.parent))

from lint.witnesslint import witnesslint

sys.dont_write_bytecode = True  # prevent creation of .pyc files

if __name__ == "__main__":
    sys.exit(witnesslint.main())
